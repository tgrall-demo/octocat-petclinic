#!/bin/bash

mkdir -p ./scans/codeql

codeql database create ./scans/codeql/java_db -s $PWD/src/main/java -l java --command "$PWD/mvnw -f $PWD/pom.xml clean compile -Dcheckstyle.skip " --overwrite

codeql database analyze \
    --format="sarif-latest" \
    --sarif-category="codeql-scan:java" \
    --output="./scans/output.sarif" \
    --sarif-add-query-help \
    --sarif-add-snippets \
    $PWD/scans/codeql/java_db \
    codeql/java-queries

    ./mvnw  clean

